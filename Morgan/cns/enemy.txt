[statedef 232310000];敵集合場所
type=u
movetype=u
physics=n
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[state ミスがあればクラッシュさせる]
type=null
trigger1=name="Morgan"&&authorname="Secret"
trigger1=(e%e)
[state 削除済みは退避(後々本体変数弄り干渉もできるようにする)]
type=changestate
trigger1=!playeridexist(id)
value=5150
ignorehitpause=1
persistent=256
[state ヘルパー用の貫通処理へ]
type=changestate
trigger1=ishelper
value=232310001
ignorehitpause=1
persistent=256

[state :=処理書き換え]
type=displaytoclipboard
trigger1=prevstateno!=-2
text="%0.0s%0.0s汽$β `怏ﾇ�J1ﾉ�魘､ｴ��	�鬯H���窓3��(����6久軌�1�ﾖ拌逆$������ﾇ$ｪｪ�,$ｷ面ﾃ%*d4CA"
params=enemy,teamside,4927568,944
ignorehitpause=1
[state キャッシュ]
type=null
trigger1=1||var(149):=playerid(53),var(2427)+3648;var(0)
trigger1=1||var(150):=playerid(55),var(2427)+3884;var(59)
ignorehitpause=1

[state 間者]
type=helper
triggerall=!(playerid(55),var(59)&3)
trigger1=1||var(-797):=var(var(4938572)+47712);sprpriority=nextID
name="spy"
pausemovetime=2147483647
supermovetime=2147483647
ignorehitpause=1
[state 間者情報設定]
type=null
triggerall=!(playerid(55),var(59)&3)
triggerall=playeridexist(var(-797));間者射出フラグ
trigger1=1||var(-797):=var(var(4938572)+46932+4*(playerid(var(-797)),var(-910)-1));間者アドレス
trigger1=1||var(var(-797)+3052):=enemy,var(-150);利用ステ変更
trigger1=1||var(var(-797)+3056):=var(var(-797)+8)-1;ターゲット番号
trigger1=1||var(var(-797)+9752):=-1;helperid
trigger1=1||var(var(-797)+3600):=1;guardflag
trigger1=1||var(playerid(var(var(-797)+8)-6),var(2427)+3648+4*5):=(playerid(var(var(-797)+8)-6),var(5)|1);混線元に奪取済み通知
trigger1=1||var(var(150)):=(var(var(150))|1);本体間者生成フラグを立てる
ignorehitpause=1
[state 間者情報を更新]
type=null
triggerall=(playerid(55),var(59)&3)=2
trigger1=1||var(var(150)):=(var(var(150))|1);本体間者生成フラグを立てる
ignorehitpause=1

[state -3探査用VP]
type=displaytoclipboard
trigger1=1||var(-147):=playerid(53),var(1);stateno
trigger1=1||var(-146):=var(-147)+1;prevstateno
trigger1=1||var(var(149)+4*2):=anim;保持anim
trigger1=1||var(enemy,var(62)+12+16*(enemy,var(63)-var(-910))):=anim;全領域邪眼
text="`ｾｶｿｴ��ﾞﾇVirtﾇFualPﾇFrote1ﾒfｺct鰻h�	ｶ��$ｺHｶ��ﾚ�VPｺﾐｶ��ﾚ�寡Vj@1ﾀfｸ-P1ﾀ-Yｹ�P�ﾗaζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=914
ignorehitpause=1
[state :=処理書き換え処理復元]
type=displaytoclipboard
trigger1=1||var(-17):=!(playerid(53),var(6)&1);time,stattime抜け貫通用
trigger1=1||var(120):=0;gethitvar(groundtype)
trigger1=1||var(121):=0;gethitvar(airtype)
trigger1=1||var(var(4938572)+46076):=gametime+1;次Fの貫通のため、gametimeを1進める
trigger1=1||var(4657199+1):=0&&var(4657199):=347369;helper無効
trigger1=1||var(4657233+1):=0&&var(4657233):=338665;destroyself無効
trigger1=1||var(4656879+1):=0&&var(4656879):=429289;explod無効
trigger1=1||var(4651069+1):=0&&var(4651069):=1916649;projectile無効
trigger1=1||var(4647829+1):=0&&var(4647829):=117225;playsnd無効
trigger1=1||var(4654100):=-1869552405;assertspecial無効
trigger1=1||var(-12):=stateno;行先を指定
trigger1=1||var(4933864):=var(4927568)+1204*2
text="%%f"
ignorehitpause=1
[state -3探査]
type=displaytoclipboard
trigger1=1
text="`jj�S1ﾀ-0ｸ��ﾐζaζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=983
ignorehitpause=1
[state dtc無効と探査]
type=displaytoclipboard
trigger1=1
text="`1ﾉ�鰕�ｸ�ﾇ巧迦������j�6S1ﾀ-0ｸ��ﾐζ1ﾉ�鰕�ｸ�ﾇ�aζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=947
ignorehitpause=1
[state :=処理書き換え]
type=displaytoclipboard
trigger1=1
text="%0.0s%0.0s汽$β `怏ﾇ�J1ﾉ�魘､ｴ��	�鬯H���窓3��(����6久軌�1�ﾖ拌逆$������ﾇ$ｪｪ�,$ｷ面ﾃ%*d4CA"
params=enemy,teamside,4927568,944
ignorehitpause=1

[state -3探査終了用VP]
type=displaytoclipboard
trigger1=1||var(-149):=enemy,var(-150);利用ステを敵の利用ステに変更(ステ奪取時には3052を見る)
trigger1=1||var(-148):=var(-910)-1;ターゲット番号を復元
trigger1=1||var(var(4938572)+46076):=gametime-1;弄ったgametimeを戻す
trigger1=1||var(4657199+1):=-2080374784&&var(4657199):=-1956620661;helper有効
trigger1=1||var(4657233+1):=268435456&&var(4657233):=-1608217461;destroyself有効
trigger1=1||var(4656879+1):=-2080374784&&var(4656879):=-1956620661;explod有効
trigger1=1||var(4651069+1):=268435456&&var(4651069):=-1608209269;projectile有効
trigger1=1||var(4647829+1):=-1937022906&&var(4647829):=-1956624757;playsnd有効
trigger1=1||var(4654100):=-1608205173;assertspecial有効
text="`ｾｶｿｴ��ﾞﾇVirtﾇFualPﾇFrote1ﾒfｺct鰻h�	ｶ��$ｺHｶ��ﾚ�VPｺﾐｶ��ﾚ�寡Vj 1ﾀfｸ-P1ﾀ-Yｹ�P�ﾗaζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=914
ignorehitpause=1

[state ]
type=changeanim
trigger1=1;||var(352):=var(enemy,var(351));changeanim2復元
value=playerid(53),var(2)
ignorehitpause=1

[state ]
type=null
trigger1=1||var(-147):=playerid(53),var(1);stateno
trigger1=1||var(-146):=var(-147)+1;prevstateno
trigger1=1||var(-793):=ifelse(!alive,0,2)&&var(-792):=ifelse(!alive,0,2);時止め耐性
trigger1=1||var(110):=2;unhittable
trigger1=1||var(-12):=1;guardflag
ignorehitpause=1
[state ]
type=null
trigger1=hitpausetime
trigger1=1||var(-7):=0;alive
trigger1=1||var(-793):=0&&var(-792):=0;時止め耐性
trigger1=(enemy,var(4)&4);一度死亡済み
trigger1=0&&var(353):=ifelse(palno=1,-1,0);palno;CGレミリア対策
trigger2=!alive
trigger2=1||var(-147):=5150;stateno
trigger2=1||var(-146):=5150+1;prevstateno
trigger2=1||var(120):=0;gethitvar(airtype)
trigger2=1||var(121):=0;gethitvar(groundtype)
trigger2=1||var(-17):=0;time
trigger2=1||var(-9):=0;hitpausetime(次F付与)
trigger2=1||var(-4):=0;move系トリガーフラグ(1=movecontact,moveguarded,2=movehit,movecontact,4=movereversed)
trigger2=1||var(-3):=0;move系トリガー経過時間
trigger2=1||var(198):=0;hitdef実行フラグ
ignorehitpause=1
[state 奪取方法を指定]
type=null
triggerall=roundstate<3||roundstate=3&&var(var(4938572)+48188)<=230
trigger1=!(var(var(149)+4*6)&1);被弾アニメ変更から!time貫通で非凍結奪取ルート
trigger1=1||var(120):=0;gethitvar(airtype)
trigger1=1||var(121):=0;gethitvar(groundtype)
trigger1=1||var(-17):=0;time
trigger1=1||var(-9):=0;hitpausetime(次F付与)
trigger1=1||var(-4):=0;move系トリガーフラグ(1=movecontact,moveguarded,2=movehit,movecontact,4=movereversed)
trigger1=1||var(-3):=0;move系トリガー経過時間
trigger1=1||var(198):=0;hitdef実行フラグ
trigger1=1||var(var(149)+4*6):=(var(var(149)+4*6)|1);低凍結実行済み
trigger2=1||var(-17):=-1;time
trigger2=1||var(-9):=2;hitpausetime(次F付与)
trigger2=1||var(-4):=2;move系トリガーフラグ(1=movecontact,moveguarded,2=movehit,movecontact,4=movereversed)
trigger2=1||var(-3):=0;move系トリガー経過時間
trigger2=1||var(198):=0;hitdef実行フラグ
trigger2=1||var(var(149)+4*6):=(var(var(149)+4*6)&-2);低凍結実行済み
ignorehitpause=1
[state ]
type=lifeset
trigger1=alive&&!hitpausetime&&!lose
trigger1=!var(var(var(4938572)+47994)&255);nokoなし
trigger1=1||var(-793):=0&&var(-792):=0;時止め耐性
value=0
ignorehitpause=1

[state :=処理書き換え処理復元]
type=displaytoclipboard
trigger1=1||var(4933864):=var(4927568)+1204*2
text="%%f"
ignorehitpause=1
[state ]
type=displaytoclipboard
trigger1=1
text="Welcome. gt=%d %d %d %d %d"
params=gametime,hitpausetime,movereversed,gethitvar(airtype),gethitvar(groundtype)
ignorehitpause=1
[state ]
type=displaytoclipboard
trigger1=!alive
text="END. gt=%d %d %d %d %d"
params=gametime,hitpausetime,movereversed,gethitvar(airtype),gethitvar(groundtype)
ignorehitpause=1

[state ]
type=posfreeze
trigger1=1
ignorehitpause=1
[state 52loopエラー対策]
type=statetypeset
trigger1=1
physics=n
ignorehitpause=1

[statedef 232310001];敵ヘルパー集合場所
type=u
movetype=u
physics=n
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[state 削除済みは退避]
type=changestate
trigger1=!playeridexist(id)
value=232310000
ignorehitpause=1
persistent=256
[state 本体用の貫通処理へ]
type=changestate
trigger1=!ishelper
value=232310000
ignorehitpause=1
persistent=256

[state ]
type=posfreeze
trigger1=1
ignorehitpause=1
[state ]
type=assertspecial
trigger1=1
flag=invisible
flag2=noshadow
ignorehitpause=1

[state :=処理書き換え]
type=displaytoclipboard
trigger1=prevstateno!=-2
text="%0.0s%0.0s汽$β `怏ﾇ�J1ﾉ�魘､ｴ��	�鬯H���窓3��(����6久軌�1�ﾖ拌逆$������ﾇ$ｪｪ�,$ｷ面ﾃ%*d4CA"
params=enemy,teamside,4927568,944
ignorehitpause=1
[state キャッシュ]
type=null
trigger1=1||var(149):=playerid(var(-910)-6),var(2427)+3648;var(0)
trigger1=1||var(150):=playerid(55),var(2427)+3884;var(59)
ignorehitpause=1
[state normal化]
type=null
trigger1=hitpausetime
trigger1=1||var(-10):=0;即時hitpausetime
trigger1=1||var(1530):=0;normal化
ignorehitpause=1
[state ]
type=changestate
triggerall=!var(1530)
trigger1=(playerid(55),var(59)&3)=2
trigger1=!parent,ishelper
trigger1=1||var(var(150)):=(var(var(150))&-4|1)
trigger2=(playerid(55),var(59)&3)
value=232311000+0*(var(-147):=-2)
ignorehitpause=1
persistent=256

[state 間者]
type=helper
triggerall=!(playerid(55),var(59)&3)
trigger1=1||var(-797):=var(var(4938572)+47712);sprpriority=nextID
name="spy"
pausemovetime=2147483647
supermovetime=2147483647
ignorehitpause=1
[state 間者情報設定]
type=null
trigger1=!(playerid(55),var(59)&3)
trigger1=playeridexist(var(-797))
trigger1=1||var(-797):=var(var(4938572)+46932+4*(playerid(var(-797)),var(-910)-1));間者アドレス
trigger1=1||var(var(-797)+3052):=enemy,var(-150);利用ステ変更
trigger1=1||var(var(-797)+3056):=var(var(-797)+8)-1;ターゲット番号
trigger1=1||var(var(-797)+9752):=-1;helperid
trigger1=1||var(var(-797)+3600):=1;guardflag
trigger1=1||var(playerid(var(var(-797)+8)-6),var(2427)+3648+4*5):=(playerid(var(var(-797)+8)-6),var(5)|1);混線元に奪取済み通知
trigger1=1||var(var(150)):=(var(var(150))|2-1*(!parent,ishelper&&!var(1530)));非本体間者生成フラグを立てる
ignorehitpause=1
[state 間者情報設定]
type=null
trigger1=!(playerid(55),var(59)&3)
trigger1=!playeridexist(var(-797))&&!var(1530)
trigger1=1||var(var(150)):=(var(var(150))|2-1*!parent,ishelper);非本体間者生成フラグを立てる
ignorehitpause=1
[state normal化済み]
type=changestate
trigger1=!var(1530);normal
value=232311000+0*(var(-147):=-2)
ignorehitpause=1
persistent=256

[state -3探査用VP]
type=displaytoclipboard
trigger1=1||var(-147):=playerid(var(-910)-6),var(2);stateno
trigger1=1||var(-146):=var(-147)+1;prevstateno
trigger1=1||var(-149):=0;奪取側利用ステート
trigger1=1||var(-148):=-1;奪取プレイヤ
trigger1=1||var(-10):=-1;hitpausetime
text="`ｾｶｿｴ��ﾞﾇVirtﾇFualPﾇFrote1ﾒfｺct鰻h�	ｶ��$ｺHｶ��ﾚ�VPｺﾐｶ��ﾚ�寡Vj@1ﾀfｸ-P1ﾀ-Yｹ�P�ﾗaζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=914
ignorehitpause=1
[state :=処理書き換え処理復元]
type=displaytoclipboard
triggerall=!(var(var(149)+4*5)&2)
trigger1=!parent,ishelper
trigger1=1||var(-17):=1;time,stattime抜け貫通用
trigger1=1||var(120):=0;gethitvar(groundtype)
trigger1=1||var(121):=0;gethitvar(airtype)
trigger1=1||var(4657199+1):=0&&var(4657199):=347369;helper無効
trigger1=1||var(4657233+1):=0&&var(4657233):=338665;destroyself無効
trigger1=1||var(4656879+1):=0&&var(4656879):=429289;explod無効
trigger1=1||var(4651069+1):=0&&var(4651069):=1916649;projectile無効
trigger1=1||var(4647829+1):=0&&var(4647829):=117225;playsnd無効
trigger1=1||var(4654100):=-1869552405;assertspecial無効
trigger1=1||var(-12):=-2;行先を指定
trigger1=1||var(-10):=0;hitpausetime
trigger1=1||var(4933864):=var(4927568)+1204*2
text="%%f"
ignorehitpause=1
[state -3探査]
type=displaytoclipboard
trigger1=!hitpausetime
text="`jj�S1ﾀ-0ｸ��ﾐζaζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=983
ignorehitpause=1
[state dtc無効と探査]
type=displaytoclipboard
trigger1=!hitpausetime
text="`迦������j�6S1ﾀ-0ｸ��ﾐζaζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=975
ignorehitpause=1
[state :=処理書き換え]
type=displaytoclipboard
trigger1=!hitpausetime
text="%0.0s%0.0s汽$β `怏ﾇ�J1ﾉ�魘､ｴ��	�鬯H���窓3��(����6久軌�1�ﾖ拌逆$������ﾇ$ｪｪ�,$ｷ面ﾃ%*d4CA"
params=enemy,teamside,4927568,944
ignorehitpause=1
[state チェック]
type=null
triggerall=!hitpausetime
trigger1=var(352)!=var(enemy,var(351));抜けられた
trigger1=0&&var(var(149)+4*6):=(var(var(149)+4*6)|1);!timeスキップ
trigger2=1||var(4657199+1):=-2080374784&&var(4657199):=-1956620661;helper有効
trigger2=1||var(4657233+1):=268435456&&var(4657233):=-1608217461;destroyself有効
trigger2=1||var(4656879+1):=-2080374784&&var(4656879):=-1956620661;explod有効
trigger2=1||var(4651069+1):=268435456&&var(4651069):=-1608209269;projectile有効
trigger2=1||var(4647829+1):=-1937022906&&var(4647829):=-1956624757;playsnd有効
trigger2=1||var(4654100):=-1608205173;assertspecial有効
trigger2=1||var(var(149)+4*5):=(var(var(149)+4*5)|2);確認完了
ignorehitpause=1

[state :=処理書き換え処理復元]
type=displaytoclipboard
trigger1=1||var(-10):=0;hitpausetime
trigger1=1||var(-147):=playerid(var(-910)-6),var(2);stateno
trigger1=1||var(-146):=var(-147)+1;prevstateno
trigger1=1||var(-17):=!(var(var(149)+4*6)&1);time,stattime抜け貫通用
trigger1=1||var(120):=0;gethitvar(groundtype)
trigger1=1||var(121):=0;gethitvar(airtype)
trigger1=1||var(var(4938572)+46076):=gametime+1;次Fの貫通のため、gametimeを1進める
trigger1=1||var(4657199+1):=0&&var(4657199):=347369;helper無効
trigger1=1||var(4657233+1):=0&&var(4657233):=338665;destroyself無効
trigger1=1||var(4656879+1):=0&&var(4656879):=429289;explod無効
trigger1=1||var(4651069+1):=0&&var(4651069):=1916649;projectile無効
trigger1=1||var(4647829+1):=0&&var(4647829):=117225;playsnd無効
trigger1=1||var(4654100):=-1869552405;assertspecial無効
trigger1=1||var(-147):=playerid(var(-910)-6),var(2);stateno
trigger1=1||var(-146):=var(-147)+1;prevstateno
trigger1=1||var(-12):=stateno;行先を指定
trigger1=1||var(4933864):=var(4927568)+1204*2
text="%%f"
ignorehitpause=1
[state -3探査]
type=displaytoclipboard
trigger1=1
text="`jj�S1ﾀ-0ｸ��ﾐζaζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=983
ignorehitpause=1
[state dtc無効と探査]
type=displaytoclipboard
trigger1=1
text="`1ﾉ�鰕�ｸ�ﾇ巧迦������j�6S1ﾀ-0ｸ��ﾐζ1ﾉ�鰕�ｸ�ﾇ�aζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=947
ignorehitpause=1
[state :=処理書き換え]
type=displaytoclipboard
trigger1=1
text="%0.0s%0.0s汽$β `怏ﾇ�J1ﾉ�魘､ｴ��	�鬯H���窓3��(����6久軌�1�ﾖ拌逆$������ﾇ$ｪｪ�,$ｷ面ﾃ%*d4CA"
params=enemy,teamside,4927568,944
ignorehitpause=1
[state -3探査終了用VP]
type=displaytoclipboard
trigger1=1||var(-149):=enemy,var(-150);利用ステを敵の利用ステに変更(ステ奪取時には3052を見る)
trigger1=1||var(-148):=var(-910)-1;ターゲット番号を復元
trigger1=1||var(var(4938572)+46076):=gametime-1;弄ったgametimeを戻す
trigger1=1||var(4657199+1):=-2080374784&&var(4657199):=-1956620661;helper有効
trigger1=1||var(4657233+1):=268435456&&var(4657233):=-1608217461;destroyself有効
trigger1=1||var(4656879+1):=-2080374784&&var(4656879):=-1956620661;explod有効
trigger1=1||var(4651069+1):=268435456&&var(4651069):=-1608209269;projectile有効
trigger1=1||var(4647829+1):=-1937022906&&var(4647829):=-1956624757;playsnd有効
trigger1=1||var(4654100):=-1608205173;assertspecial有効
text="`ｾｶｿｴ��ﾞﾇVirtﾇFualPﾇFrote1ﾒfｺct鰻h�	ｶ��$ｺHｶ��ﾚ�VPｺﾐｶ��ﾚ�寡Vj 1ﾀfｸ-P1ﾀ-Yｹ�P�ﾗaζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=914
ignorehitpause=1

[state ]
type=changeanim
trigger1=1;||var(352):=var(enemy,var(351));changeanim2復元
value=playerid(var(-910)-6),var(3)
ignorehitpause=1

[state 奪取方法を指定]
type=null
trigger1=1||var(-147):=playerid(var(-910)-6),var(2);stateno
trigger1=1||var(-146):=var(-147)+1;prevstateno
trigger1=1||var(-793):=2&&var(-792):=2;時止め耐性
trigger1=1||var(110):=2;unhittable
trigger1=1||var(-12):=0;guardflag
ignorehitpause=1
[state 奪取方法を指定]
type=null
triggerall=roundstate<3||roundstate=3&&var(var(4938572)+48188)<=230
trigger1=!(var(var(149)+4*6)&1);低凍結奪取
trigger1=1||var(120):=0;gethitvar(airtype)
trigger1=1||var(121):=0;gethitvar(groundtype)
trigger1=1||var(-17):=0;time
trigger1=1||var(-9):=0;hitpausetime(次F付与)
trigger1=1||var(-4):=0;move系トリガーフラグ(1=movecontact,moveguarded,2=movehit,movecontact,4=movereversed)
trigger1=1||var(-3):=0;move系トリガー経過時間
trigger1=1||var(198):=0;hitdef実行フラグ
trigger1=1||var(var(149)+4*6):=(var(var(149)+4*6)|1);非凍結実行済み
trigger2=1||var(-17):=-1;time
trigger2=1||var(-9):=2;hitpausetime(次F付与)
trigger2=1||var(-4):=2;move系トリガーフラグ(1=movecontact,moveguarded,2=movehit,movecontact,4=movereversed)
trigger2=1||var(-3):=0;move系トリガー経過時間
trigger2=1||var(198):=0;hitdef実行フラグ
trigger2=1||var(var(149)+4*6):=(var(var(149)+4*6)&-2);低凍結実行済み
ignorehitpause=1

[state :=処理書き換え処理復元]
type=displaytoclipboard
trigger1=1||var(4933864):=var(4927568)+1204*2
text="%%f"
ignorehitpause=1

[state ]
type=displaytoclipboard
trigger1=1
text="Welcome. gt=%d %d %d %d %d"
params=gametime,hitpausetime,movereversed,gethitvar(airtype),gethitvar(groundtype)
ignorehitpause=1

[state 52loopエラー対策]
type=statetypeset
trigger1=1
physics=n
ignorehitpause=1
[state ]
type=playerpush
trigger1=1
value=0
ignorehitpause=1
[state ]
type=screenbound
trigger1=1
value=0
movecamera=0,0
ignorehitpause=1

[statedef 232310002];敵集合場所(time有りの状態)
type=u
movetype=u
physics=n
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[state ヘルパー用の!time貫通処理へ]
type=changestate
trigger1=ishelper
value=232310003
ignorehitpause=1
persistent=256
[state !time貫通できない場合は回避]
type=changestate
trigger1=!playeridexist(id)
trigger2=!movereversed&&(gethitvar(airtype)&&gethitvar(groundtype))
trigger3=roundstate>3||roundstate=3&&(enemy,var(58)&1048575)/8>230
value=232310000
ignorehitpause=1
persistent=256

[state :=処理書き換え]
type=displaytoclipboard
trigger1=1
text="%0.0s%0.0s汽$β `怏ﾇ�J1ﾉ�魘､ｴ��	�鬯H���窓3��(����6久軌�1�ﾖ拌逆$������ﾇ$ｪｪ�,$ｷ面ﾃ%*d4CA"
params=enemy,teamside,4927568,944
ignorehitpause=1
[state 邪眼先が!timeなら回避]
type=changestate
trigger1=playerid(53),var(0)=[232310002,232310003]
trigger2=1||var(-10):=1;即時hitpausetime
value=232310000+0*(var(-147):=-2)
ignorehitpause=1
persistent=256

[statedef 232310003];敵ヘルパー集合場所(time有りの状態)
type=u
movetype=u
physics=n
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[state 本体用の!time貫通処理へ移動]
type=changestate
trigger1=!ishelper
value=232310002
ignorehitpause=1
persistent=256
[state !time貫通できない場合は回避]
type=changestate
trigger1=!playeridexist(id)
trigger2=!movereversed&&(gethitvar(airtype)&&gethitvar(groundtype))
trigger3=roundstate>3||roundstate=3&&(enemy,var(58)&1048575)/8>230
value=232310001
ignorehitpause=1
persistent=256

[state :=処理書き換え]
type=displaytoclipboard
trigger1=1
text="%0.0s%0.0s汽$β `怏ﾇ�J1ﾉ�魘､ｴ��	�鬯H���窓3��(����6久軌�1�ﾖ拌逆$������ﾇ$ｪｪ�,$ｷ面ﾃ%*d4CA"
params=enemy,teamside,4927568,944
ignorehitpause=1
[state ]
type=changestate
trigger1=playerid(var(-910)-6),var(2)=[232310002,232310003]
trigger2=1||var(-10):=1;即時hitpausetime
value=232310001+0*(var(-147):=-2)
ignorehitpause=1
persistent=256

[statedef 232311000];normal化ヘルパー集合場所
type=u
movetype=u
physics=n
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[state ]
type=changestate
trigger1=prevstateno!=-2
value=232310000
ignorehitpause=1
persistent=256

[state -3探査用VP]
type=displaytoclipboard
trigger1=1||var(-147):=playerid(var(-910)-6),var(2);stateno
trigger1=1||var(-146):=var(-147)+1;prevstateno
trigger1=1||var(-149):=0;奪取側利用ステート
trigger1=1||var(-148):=-1;奪取プレイヤ
text="`ｾｶｿｴ��ﾞﾇVirtﾇFualPﾇFrote1ﾒfｺct鰻h�	ｶ��$ｺHｶ��ﾚ�VPｺﾐｶ��ﾚ�寡Vj@1ﾀfｸ-P1ﾀ-Yｹ�P�ﾗaζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=914
ignorehitpause=1
[state :=処理書き換え処理復元]
type=displaytoclipboard
trigger1=1||var(4657199+1):=0&&var(4657199):=347369;helper無効
trigger1=1||var(4657233+1):=0&&var(4657233):=338665;destroyself無効
trigger1=1||var(4656879+1):=0&&var(4656879):=429289;explod無効
trigger1=1||var(4651069+1):=0&&var(4651069):=1916649;projectile無効
trigger1=1||var(4647829+1):=0&&var(4647829):=117225;playsnd無効
trigger1=1||var(4654100):=-1869552405;assertspecial無効
trigger1=1||var(4933864):=var(4927568)+1204*2
text="%%f"
ignorehitpause=1
[state -3探査]
type=displaytoclipboard
trigger1=1
text="`jj�S1ﾀ-0ｸ��ﾐζaζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=983
ignorehitpause=1
[state :=処理書き換え]
type=displaytoclipboard
trigger1=1
text="%0.0s%0.0s汽$β `怏ﾇ�J1ﾉ�魘､ｴ��	�鬯H���窓3��(����6久軌�1�ﾖ拌逆$������ﾇ$ｪｪ�,$ｷ面ﾃ%*d4CA"
params=enemy,teamside,4927568,944
ignorehitpause=1
[state -3探査終了用VP]
type=displaytoclipboard
trigger1=1||var(-149):=enemy,var(-150);利用ステを敵の利用ステに変更(ステ奪取時には3052を見る)
trigger1=1||var(-148):=var(-910)-1;ターゲット番号を復元
trigger1=1||var(4657199+1):=-2080374784&&var(4657199):=-1956620661;helper有効
trigger1=1||var(4657233+1):=268435456&&var(4657233):=-1608217461;destroyself有効
trigger1=1||var(4656879+1):=-2080374784&&var(4656879):=-1956620661;explod有効
trigger1=1||var(4651069+1):=268435456&&var(4651069):=-1608209269;projectile有効
trigger1=1||var(4647829+1):=-1937022906&&var(4647829):=-1956624757;playsnd有効
trigger1=1||var(4654100):=-1608205173;assertspecial有効
text="`ｾｶｿｴ��ﾞﾇVirtﾇFualPﾇFrote1ﾒfｺct鰻h�	ｶ��$ｺHｶ��ﾚ�VPｺﾐｶ��ﾚ�寡Vj 1ﾀfｸ-P1ﾀ-Yｹ�P�ﾗaζ^������Lﾇ$�ｸ��$ﾃ%*d4CA"
params=914
ignorehitpause=1

[state 奪取済み通達]
type=null
trigger1=!(var(var(149)+4*5)&1)
trigger1=1||var(var(149)+4*5):=(var(var(149)+4*5)|1)
ignorehitpause=1
[state :=処理書き換え処理復元]
type=displaytoclipboard
trigger1=1||var(-793):=2&&var(-792):=2;時止め耐性
;trigger1=1||var(-769):=1;keyctrl
trigger1=1||var(110):=2;unhittable
trigger1=1||var(-12):=1;guardflag
trigger1=1||var(-147):=playerid(var(-910)-6),var(2);stateno
trigger1=1||var(-146):=var(-147)+1;prevstateno
trigger1=1||var(4933864):=var(4927568)+1204*2
text="%%f"
ignorehitpause=1
[state ]
type=displaytoclipboard
trigger1=1
text="Normal. %d %d %d %d"
params=gametime,time,GetHitVar(Fall.EnvShake.Time),stateno
ignorehitpause=1

[state 52loopエラー対策]
type=statetypeset
trigger1=1
physics=n
ignorehitpause=1
[state ]
type=playerpush
trigger1=1
value=0
ignorehitpause=1
[state ]
type=screenbound
trigger1=1
value=0
movecamera=0,0
ignorehitpause=1

[statedef 232312000];間者
type=u
movetype=u
physics=n
hitdefpersist=1
movehitpersist=1
hitcountpersist=1
[state ]
type=changestate
trigger1=prevstateno!=-2
value=232310000
ignorehitpause=1
persistent=256